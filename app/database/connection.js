const  mysql = require('mysql');
import { database } from './keys.js'
const { promisify } = require ('util');

const pool = mysql.createPool(database)

pool.getConnection( (err, connection) => {
    if(err) {
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.log('Se perdió la conexión a a la base de datos');
        }
        if (err.code = 'ER_CON_COUNT_ERROR') {
            console.log('Demasiadas conexiones');
        }
        if (err.code = 'ECONNREFUSED') {
            console.log('Conexión rechazada')
        }
        console.log('Hay un error');
    }

    if(connection) connection.release();
    console.log('Conectado');
    return;
})

pool.query = promisify(pool.query);

export {pool};
