CREATE SCHEMA IF NOT EXISTS `proyecto` DEFAULT CHARACTER SET utf8;
USE `proyecto` ;

CREATE TABLE IF NOT EXISTS `categorias` (
  `idCategoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45),
  `bandera` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idCategoria`));

CREATE TABLE IF NOT EXISTS `codpostal` (
  `idCodPostal` INT(11) NOT NULL AUTO_INCREMENT,
  `CodPostal` VARCHAR(45),
  PRIMARY KEY (`idCodPostal`));

CREATE TABLE IF NOT EXISTS `edo` (
  `idEdo` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEdo`));

CREATE TABLE IF NOT EXISTS `clientes` (
  `idClientes` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Ape_Pa` VARCHAR(45),
  `Ape_Ma` VARCHAR(45),
  `RFC` VARCHAR(45) NOT NULL,
  `Telefono` VARCHAR(45),
  `idCodPostal` INT(11),
  `idEdo` INT(11),
  `Celular` VARCHAR(45),
  `Observaciones` VARCHAR(500),
  `Dir` VARCHAR(60),
  `activo` INT,
  PRIMARY KEY (`idClientes`, `idCodPostal`, `idEdo`),
  CONSTRAINT `fk_CLIENTES_CODPOSTAL1`
    FOREIGN KEY (`idCodPostal`)
    REFERENCES `codpostal` (`idCodPostal`),
  CONSTRAINT `fk_CLIENTES_EDO1`
    FOREIGN KEY (`idEdo`)
    REFERENCES `edo` (`idEdo`));

CREATE TABLE IF NOT EXISTS `productos` (
  `idPRODUCTOS` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45),
  `precioVenta` INT NOT NULL,
  `precioCompra` INT NOT NULL,
  `activo` INT,
  PRIMARY KEY (`idPRODUCTOS`));

CREATE TABLE IF NOT EXISTS `proveedores` (
  `idProveedor` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45),
  `telefono` VARCHAR(45),
  `celular` VARCHAR(45),
  `RFC` VARCHAR(45),
  `Correo` VARCHAR(45),
  `observaciones` VARCHAR(500),
  `activo` INT,
  PRIMARY KEY (`idProveedor`));

CREATE TABLE IF NOT EXISTS `compras` (
  `idCompras` INT NOT NULL AUTO_INCREMENT,
  `idProveedor` INT(11) NOT NULL,
  `fecha` DATETIME NOT NULL,
  PRIMARY KEY (`idCompras`),
  CONSTRAINT `fk_PROVEEDORES_has_PRODUCTOS_PROVEEDORES1`
    FOREIGN KEY (`idProveedor`)
    REFERENCES `proveedores` (`idProveedor`));

CREATE TABLE IF NOT EXISTS `empleados` (
  `idEmpleado` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45),
  `Ape_Pa` VARCHAR(45),
  `Ape_Ma` VARCHAR(45),
  `activo` INT,
  PRIMARY KEY (`idEmpleado`));

CREATE TABLE IF NOT EXISTS `paqueterias` (
  `idPaqueteria` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `precio_kilo` INT(11) NOT NULL,
  `observaciones` VARCHAR(500),
  `activo` INT,
  PRIMARY KEY (`idPaqueteria`));

CREATE TABLE IF NOT EXISTS `paq_clientes` (
  `idPaqueteria` INT(11) NOT NULL,
  `idClientes` INT(11) NOT NULL,
  PRIMARY KEY (`idPaqueteria`, `idClientes`),
  CONSTRAINT `fk_PAQUETERIAS_has_CLIENTES_CLIENTES1`
    FOREIGN KEY (`idClientes`)
    REFERENCES `clientes` (`idClientes`),
  CONSTRAINT `fk_PAQUETERIAS_has_CLIENTES_PAQUETERIAS`
    FOREIGN KEY (`idPaqueteria`)
    REFERENCES `paqueterias` (`idPaqueteria`));

CREATE TABLE IF NOT EXISTS `produc_cat` (
  `idProducto` INT(11) NOT NULL,
  `idCategoria` INT(11) NOT NULL,
  PRIMARY KEY (`idProducto`, `idCategoria`),
  CONSTRAINT `fk_PRODUCTOS_has_CATEGORIAS_CATEGORIAS1`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `categorias` (`idCategoria`),
  CONSTRAINT `fk_PRODUCTOS_has_CATEGORIAS_PRODUCTOS1`
    FOREIGN KEY (`idProducto`)
    REFERENCES `productos` (`idPRODUCTOS`));

CREATE TABLE IF NOT EXISTS `ventas` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT(11) NOT NULL,
  `fecha` DATETIME,
  `idEmpleado` INT(11) NOT NULL,
  PRIMARY KEY (`idVenta`),
  CONSTRAINT `fk_CLIENTES_has_PRODUCTOS_CLIENTES1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `clientes` (`idClientes`),
  CONSTRAINT `fk_Ventas_EMPLEADOS1`
    FOREIGN KEY (`idEmpleado`)
    REFERENCES `empleados` (`idEmpleado`));

CREATE TABLE IF NOT EXISTS `Tallas` (
  `idTallas` INT NOT NULL AUTO_INCREMENT,
  `talla` CHAR,
  PRIMARY KEY (`idTallas`));

CREATE TABLE IF NOT EXISTS `productos_Tallas` (
  `productos_idPRODUCTOS` INT(11) NOT NULL,
  `Tallas_idTallas` INT NOT NULL,
  PRIMARY KEY (`productos_idPRODUCTOS`, `Tallas_idTallas`),
  CONSTRAINT `fk_productos_has_Tallas_productos1`
    FOREIGN KEY (`productos_idPRODUCTOS`)
    REFERENCES `productos` (`idPRODUCTOS`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_Tallas_Tallas1`
    FOREIGN KEY (`Tallas_idTallas`)
    REFERENCES `Tallas` (`idTallas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `venta_prod` (
  `idVenta` INT NOT NULL,
  `idProductos` INT(11) NOT NULL,
  `cantidad` INT NOT NULL,
  PRIMARY KEY (`idVenta`,`idProductos`),
  CONSTRAINT `fk_ventas_has_prod_ventas` 
    FOREIGN KEY(`idVenta`)
    REFERENCES `ventas`(`idVenta`),
  CONSTRAINT `fk_ventas_has_prod_producto`
    FOREIGN KEY(`idProductos`)
    REFERENCES `productos` (`idPRODUCTOS`)
  );

CREATE TABLE IF NOT EXISTS `compra_prod` (
  `idCompra` INT NOT NULL,
  `idProductos` INT(11) NOT NULL,
  `cantidad` INT NOT NULL,
  PRIMARY KEY (`idCompra`,`idProductos`),
  CONSTRAINT `fk_compras_has_prod_compra` 
    FOREIGN KEY(`idCompra`)
    REFERENCES `compras`(`idCompras`),
  CONSTRAINT `fk_compras_has_prod_producto`
    FOREIGN KEY(`idProductos`)
    REFERENCES `productos` (`idPRODUCTOS`)
  );

CREATE TABLE IF NOT EXISTS `INVENTARIO` ( 
	`idProductos` INT(11) NOT NULL,
  `cantidad` INT NOT NULL,
  PRIMARY KEY (`idPRODUCTOS`,`cantidad`),
  CONSTRAINT `fk_inventario_has_prod` 
    FOREIGN KEY(`idProductos`) REFERENCES `productos`(`idProductos`)
);

/*INDEX*/
CREATE UNIQUE INDEX index_producto ON productos(idPRODUCTOS);

/*VIEWS*/
CREATE OR REPLACE VIEW view_clientes AS
	SELECT clientes.idClientes, CONCAT_WS(' ',clientes.Nombre, clientes.Ape_Pa, clientes.Ape_Ma) AS Nombre, 
    RFC, Telefono, Dir, clientes.Observaciones, clientes.activo, paqueterias.Nombre AS paqueteria
  FROM proyecto.clientes, proyecto.paq_clientes, proyecto.paqueterias 
  WHERE  clientes.idClientes = paq_clientes.idClientes AND paqueterias.idPaqueteria = paq_clientes.idPaqueteria;

CREATE OR REPLACE VIEW view_proveedores AS 
SELECT idProveedor, nombre, RFC, telefono, Correo, observaciones, activo From proveedores;

CREATE OR REPLACE VIEW view_productos AS 
	SELECT productos.idPRODUCTOS, productos.nombre, productos.descripcion, productos.precioVenta, productos.precioCompra, INVENTARIO.cantidad, productos.activo, Tallas.talla FROM productos, Tallas, INVENTARIO WHERE Tallas.idTallas = productos.idTalla AND productos.idPRODUCTOS = INVENTARIO.idProductos ;
/*Procedures*/
DELIMITER //
CREATE PROCEDURE getAllUser()
BEGIN
	SELECT * FROM view_clientes WHERE ACTIVO=1;
END//
DELIMITER //
CREATE PROCEDURE getAllProductos()
BEGIN
	SELECT * FROM view_proveedores WHERE ACTIVO=1;
END//
DELIMITER //
CREATE PROCEDURE getAllProductos()
BEGIN
	SELECT * FROM view_productos WHERE ACTIVO=1;
END//

/*Triggers*/
DELIMITER //
CREATE TRIGGER aumentarInventario
  AFTER INSERT ON compra_prod FOR EACH ROW
BEGIN
  UPDATE INVENTARIO SET cantidad = cantidad + NEW.cantidad WHERE idProductos = NEW.idProductos;
END//

DELIMITER //
CREATE TRIGGER disminuirInventario
  AFTER INSERT ON venta_prod FOR EACH ROW
BEGIN
  UPDATE INVENTARIO SET cantidad = cantidad - NEW.cantidad WHERE idProductos = NEW.idProductos;
END//

/*Reportes*/
SELECT 
        `c`.`Nombre` AS `Nombre`,
        `c`.`Ape_Pa` AS `Ape_Pa`,
        `c`.`Ape_Ma` AS `Ape_Ma`,
        `e`.`nombre` AS `Estado`,
        `cp`.`CodPostal` AS `CP`
    FROM
        ((`clientes` `c`
        JOIN `codpostal` `cp` ON ((`cp`.`idCodPostal` = `c`.`idCodPostal`)))
        JOIN `edo` `e` ON ((`e`.`idEdo` = `c`.`idEdo`)))
    WHERE
        (`c`.`idClientes` = CLIENTEMASVENTAS())
SELECT `p`.`nombre` AS `nombre`,
        `a`.`cantidad` AS `cantidad`,
        `t`.`talla` AS `talla`,
        `p`.`activo` AS `activo`
FROM ((`productos` `p`
      JOIN `INVENTARIO` `a` ON ((`a`.`idProductos` = `p`.`idPRODUCTOS`)))
      JOIN `Tallas` `t` ON ((`t`.`idTallas` = `p`.`idTalla`)))
WHERE (`p`.`activo` = 1) 
  UNION 
SELECT `p`.`nombre` AS `nombre`,
        `a`.`cantidad` AS `cantidad`,
        `t`.`talla` AS `talla`,
        `p`.`activo` AS `activo`
  FROM((`productos` `p`
    JOIN `INVENTARIO` `a` ON ((`a`.`idProductos` = `p`.`idPRODUCTOS`)))
    JOIN `Tallas` `t` ON ((`t`.`idTallas` = `p`.`idTalla`)))
      WHERE (`p`.`activo` = 0)

SELECT productos.nombre, sum(venta_prod.cantidad) AS total,(sum(venta_prod.cantidad)*productos.precioVenta) AS ingreso
  FROM productos, venta_prod, ventas
    WHERE productos.idPRODUCTOS = venta_prod.idProductos 
      AND ventas.idVenta = venta_prod.idVenta
      AND ventas.fecha 
          BETWEEN CAST('${fecha} 00:00:00' AS DATETIME) 
              AND CAST('${fecha} 23:59:59' AS DATETIME)
  GROUP BY productos.idPRODUCTOS, productos.nombre;

SELECT productos.nombre, sum(venta_prod.cantidad) AS total
  FROM productos, venta_prod, ventas
    WHERE productos.idPRODUCTOS = venta_prod.idProductos 
      AND ventas.idVenta = venta_prod.idVenta
GROUP BY productos.idPRODUCTOS, productos.nombre with rollup;

/*EDO*/
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Aguascalientes');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Baja California');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Baja California Sur');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Campeche');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Coahuila de Zaragoza');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Colima');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Chiapas');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Chihuahua');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Distrito Federal');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Durango');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Guanajuato');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Guerrero');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Hidalgo');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Jalisco');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('México');
INSERT INTO `proyecto`.`edo` (`nombre`) VALUES ('Michoacán de Ocampo');

/*Codigos postales*/
INSERT INTO `proyecto`.`codpostal`(`CodPostal`)  VALUES ('20100');
INSERT INTO `proyecto`.`codpostal`(`CodPostal`)  VALUES ('20110');
INSERT INTO `proyecto`.`codpostal`(`CodPostal`)  VALUES ('20120');
INSERT INTO `proyecto`.`codpostal`(`CodPostal`)  VALUES ('20130');
INSERT INTO `proyecto`.`codpostal`(`CodPostal`)  VALUES ('20140');
INSERT INTO `proyecto`.`codpostal`(`CodPostal`)  VALUES ('20150');

/*clientes*/
INSERT INTO `proyecto`.`clientes`(Nombre, Ape_Pa, Ape_Ma, RFC, Telefono, idCodPostal, idEdo, Celular, Observaciones,Dir)  VALUES('Lalo','Lita','Yala','RFC','12345678',1,1,'12345678','No le gusta el color rosa','su casa',1);
INSERT INTO `proyecto`.`proveedores` (nombre,telefono,celular,RFC,correo,observaciones,activo) VALUES("test1","tel1","cel1",'rfc',"correo1","observacion chida", 1);
insert into productos (nombre,descripcion,precioVenta,precioCompra,cantidad, activo) values ('Producto1','descProducto1',200,150,10,1);
insert into productos (nombre,descripcion,precioVenta,precioCompra,cantidad, activo) values ('Producto2','descProducto2',200,150,10,1);
insert into productos (nombre,descripcion,precioVenta,precioCompra,cantidad, activo) values ('Producto3','descProducto3',200,150,10,1);
insert into productos (nombre,descripcion,precioVenta,precioCompra,cantidad, activo) values ('Producto4','descProducto4',200,150,10,1);
insert into productos (nombre,descripcion,precioVenta,precioCompra,cantidad, activo) values ('Producto5','descProducto5',200,150,10,1);

