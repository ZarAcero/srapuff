import { app } from '../moduleAngular.js';


app.controller('proveedoresController', function ($scope, $location, apiProveedor) {

    $scope.proveedorModal = {
        idProveedor: '', nombre: '', Telefono: '', Celular: '', RFC: '', Correo: '', Observaciones: '', Activo: ''
    }
    $scope.proveedor = {
        idProveedor: '', nombre: '', Telefono: '', Celular: '', RFC: '', Correo: '', Observaciones: '' , Activo: ''
    }
    $scope.proveedores=[];
    apiProveedor.getAllProveedores().then(function (response) {
        var textLoad = document.getElementById('cargando');
        $scope.proveedores = response[0];
        textLoad.classList.add("d-none");
        $scope.$apply()
    });

    $scope.mostrarAlerta = false

    function validate(proveedor) {
        return (
            proveedor.nombre === '' ||
            proveedor.RFC === '' ||
            proveedor.correo === ''
        ) ? false : true;
    }

    $scope.agregarProveedor = function () {
        let formAgregar = document.getElementById('collapseAddProveedor')
        formAgregar.classList.remove('show');
        var textLoad = document.getElementById('cargando');
       
        if(textLoad.classList.contains("d-none")) textLoad.classList.remove("d-none"); 
        if (validate($scope.proveedor)) {
            apiProveedor.insertProveedor($scope.proveedor).then(function (response) {
                $scope.proveedor = {
                    idProveedor: '', nombre: '', Telefono: '', Celular: '', RFC: '', Correo: '', Observaciones: '' ,
                }
                apiProveedor.getAllProveedores().then(function (response) {
                    $scope.proveedores = response[0];
                    textLoad.classList.add("d-none");
                    $scope.$apply();
                })
            }, function (err) {
                console.log(err);
            })

        } else {
            alert('Llena los campos!');
        }
    }

    $scope.eliminarProveedor = function (id) {
        var textLoad = document.getElementById('cargando');
            if(textLoad.classList.contains("d-none")) textLoad.classList.remove("d-none");          
        apiProveedor.deleteProveedor(id).then(function (response) {
            apiProveedor.getAllProveedores().then(function (response) {
                textLoad.classList.add("d-none");
                $scope.proveedores = response[0]
                $scope.$apply();
            });
        });
    }

    $scope.mostrarDatosModal = function (id) {
        apiProveedor.getProveedor(id).then(function (response) {
            $scope.proveedorModal = response[0];
            $scope.$apply(function () {
                $('#editarProveedorModal').modal('show')
            });
        });
    }

    $scope.updateProveedor = function () {
        var textLoad = document.getElementById('cargando');
            if(textLoad.classList.contains("d-none")) textLoad.classList.remove(".d-none");
        if (!validate($scope.proveedorModal)) {
            $scope.mostrarAlerta = true
        } else {
            apiProveedor.updateProveedor($scope.proveedorModal).then(function (response) {
                apiProveedor.getAllProveedores().then(function (response) {
                    $scope.proveedores = response[0];
                    $('#editarProveedorModal').modal('hide');
                    $scope.$apply()
                });
            });
        }
    }
});

