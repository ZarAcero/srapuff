import { app } from '../moduleAngular.js';

app.controller('empleadosController', function ($scope,apiEmpleados) {
    
    $scope.nombre;
    $scope.apellidoPaterno;
    $scope.apellidoMaterno;
    $scope.empleados;
    $scope.clienteModal;

    $scope.agregarEmpleado=function(){
        apiEmpleados.postEmpleado([$scope.nombre,$scope.apellidoPaterno,$scope.apellidoMaterno]).then(()=>{
            apiEmpleados.getAllEmpleados().then((response)=>{
                $scope.empleados = response;
                $scope.nombre = '';
                $scope.apellidoPaterno = '';
                $scope.apellidoMaterno = '';
                $scope.$apply();
            });
        });

    }

    $scope.editar = function(){
        apiEmpleados.putEmpleado([$scope.clienteModal.nombre,$scope.clienteModal.Ape_Pa,$scope.clienteModal.Ape_Ma,$scope.clienteModal.idEmpleado]).then(()=>{
            apiEmpleados.getAllEmpleados().then((response)=>{
                $scope.empleados = response;
                $('#editarEmpleadoModal').modal('hide');
                $scope.$apply();
            });
        });
    }

    $scope.eliminar = function(id){
        apiEmpleados.deleteEmpleado(id).then((response)=>{
            apiEmpleados.getAllEmpleados().then((response)=>{
                $scope.empleados = response;
                $scope.$apply();
            }); 
        });
    }

    apiEmpleados.getAllEmpleados().then((response)=>{
        $scope.empleados = response;
        $scope.$apply();
    }); 

    $scope.mostrarDatosModal = function (id) {
        apiEmpleados.getEmpleado(id).then(function (response) {
            $scope.clienteModal = response[0];
            console.log($scope.clienteModal);
            $scope.$apply();
            $('#editarEmpleadoModal').modal('show');
        })
    }

});
