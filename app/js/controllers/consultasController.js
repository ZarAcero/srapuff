import { app } from '../moduleAngular.js';

app.controller('consultasController', function ($scope,apiConsultas) {
    console.log("inicio controlador consultas")
    $scope.productos;
    $scope.clientes;
    $scope.ventas= [];
    $scope.ventasGlobales = [];

    apiConsultas.masVentasCliente().then((response)=>{
        console.log("ventas")
        $scope.clientes = response;
        $scope.$apply();
    });

    apiConsultas.todosLosProductos().then((reponse)=>{
        console.log("clientes")
        $scope.productos = reponse;
        $scope.$apply();
    });
  
    apiConsultas.getVentas().then((response)=>{
        $scope.ventas = response;
        $scope.$apply();
    });

    apiConsultas.getVentasGlobales().then((response)=>{
        response.forEach(element => {
            if(element.nombre == null){
                element.nombre = "SubTotal";
            }
        });
        console.log(response.length);
        $scope.ventasGlobales = response;
        $scope.$apply();
    });

    $scope.getTotalIngresos = function(){
        return $scope.ventas.length > 0 ? $scope.ventas.reduce((acc,curr)=>acc+curr.ingreso,0) : 0 ;
    }

    $scope.getVentasTotales = function(){
        return $scope.ventas.length > 0 ? $scope.ventas.reduce((acc,curr)=>acc+curr.total,0) : 0 ;
    }

})