import { app } from '../moduleAngular.js';

app.controller('productosController', function ($scope, apiProductos, apiTallas) {
    console.log("inicio controladorProductos");

    $scope.productoEliminar = {
        id: '',
        nombre: ''
    }

    $scope.productoModal = {
        idPRODUCTOS: '', nombre: '', descripcion: '', idTalla: '', precioVenta: '', precioCompra: '', cantidad: '', activo: ''
    }
    $scope.productos = {
        idPRODUCTOS: '', nombre: '', descripcion: '', talla: '', precioVenta: '', precioCompra: '', cantidad: '', activo: ''
    }
    $scope.producto = {
        idPRODUCTOS: '', nombre: '', descripcion: '', idTalla: '', precioVenta: '', precioCompra: '', cantidad: '', activo: ''
    }
    $scope.tallas;

    apiTallas.getAllTallas().then(function (response) {
        console.log(response)
        $scope.tallas = response;
        $scope.$apply();
    })

    apiProductos.getProductos().then(function (response) {
        var textLoad = document.getElementById('cargando');
        var tProductos = document.getElementById('productos')
        $scope.productos = response[0];
        console.log(response[0]);
        textLoad.classList.add("d-none");
        tProductos.classList.remove("d-none");
        $scope.$apply()
    })

    $scope.mostrarAlerta = false

    /*
        $scope.eliminarProveedor = function (id) {
            var textLoad = document.getElementById('cargando');
                if(textLoad.classList.contains("d-none")) textLoad.classList.remove("d-none");          
            apiProveedor.deleteProveedor(id).then(function (response) {
                console.log(response)
                apiProveedor.getAllProveedores().then(function (response) {
                    textLoad.classList.add("d-none");
                    $scope.proveedores = response
                    $scope.$apply();
                })
            })
        }
    
        */

    $scope.eliminarProducto = function (id) {
        apiProductos.deleteProducto(id).then(function (response) {
            console.log(response);
            actualizarLista();
            $scope.$apply();
            $('#borrarProductoModal').modal('hide')
        })
    }

    $scope.mostrarBorrarModal = function (id, nombreProducto) {
        $scope.productoEliminar.nombre = nombreProducto;
        $scope.productoEliminar.id = id;

        $('#borrarProductoModal').modal('show')
    }

    $scope.agregarProducto = function () {
        let formAgregar = document.getElementById('collapseAddProducto')
        formAgregar.classList.remove('show');

        if (validate($scope.producto)) {
            console.log($scope.producto);
            apiProductos.insertProducto($scope.producto).then(function (response) {
                console.log(response);
                $scope.producto = {
                    idPRODUCTOS: '', nombre: '', descripcion: '', idTalla: '', precioVenta: '', precioCompra: '', cantidad: '', activo: ''
                }
                actualizarLista();
                $scope.$apply();
            }, function (err) {
                console.log(err);
            })
        } else {
            alert('Ingresa todos los datos necesarios');
        }
    }

    $scope.mostrarDatosModal = function (id) {
        console.log("id: " + id)
        apiProductos.getProducto(id).then(function (response) {
            console.log(response)
            $scope.productoModal = response[0]
            $scope.productoModal.idPRODUCTOS = id;
            console.log($scope.productoModal)
            $scope.$apply(function () {
                $('#editarProductosModal').modal('show')
            })
        })
    }

    $scope.updateProducto = function () {
        if (validate($scope.productoModal)) {
            console.log('XD', $scope.productoModal);
            apiProductos.updateProducto($scope.productoModal).then(function (response) {
                $('#editarProductosModal').modal('hide')
                actualizarLista();
                $scope.$apply();
            }, function (err) {
                console.log('err', err);
            })
        } else {
            $scope.mostrarAlerta = true;
        }
    }

    function actualizarLista() {
        let result = apiProductos.getProductos();
        console.log(JSON.stringify(result));
        result.then(function (response) {
            var textLoad = document.getElementById('cargando');
            var tProductos = document.getElementById('productos')
            $scope.productos = response[0];
            console.log(response[0]);
            textLoad.classList.add("d-none");
            tProductos.classList.remove("d-none");
            $scope.$apply()
        })
    }

    function validate(producto) {
        return (
            producto.nombre === '' ||
            producto.descripcion === '' ||
            producto.idTalla === '' ||
            producto.precioVenta === '' ||
            producto.precioCosto === '' ||
            producto.cantidad === ''
        ) ? false : true;
    }


    /*
        $scope.updateProveedor = function () {
            var textLoad = document.getElementById('cargando');
                if(textLoad.classList.contains("d-none")) textLoad.classList.remove(".d-none");
            if (!validate($scope.proveedoresModal)) {
                $scope.mostrarAlerta = true
            } else {
                apiProveedor.updateProveedor($scope.proveedorModal).then(function (response) {
                    console.log(response)
                    location.reload()
                    $('#editarProveedorModal').modal('hide')
                    textLoad.classList.add("d-none")
                    $scope.$apply()
                })
            }
        }
    */

});