import { app } from '../moduleAngular.js';

app.controller('ventasController', function ($scope, $window, apiProductos, apiCliente, apiEmpleados, apiVentas) {
    console.log("inicio controladorVentas");

    $scope.notaVenta = [];
    $scope.producto;
    $scope.cantidadMaxima = 0;
    $scope.productos;
    $scope.clientes;
    $scope.idCliente;
    $scope.empleados;
    $scope.idEmpleado;

    $scope.agregarProducto = function () {
        apiProductos.getProducto($scope.producto.id).then((response) => {
            $scope.notaVenta.push({
                id: $scope.producto.id,
                nombre: response[0].nombre,
                descripcion: response[0].descripcion,
                precioVenta: response[0].precioVenta,
                cantidad: $scope.producto.cantidad
            });
            $scope.producto = {}
            $scope.ventasModalForm.$setUntouched()
            $scope.$apply();
        });
    }

    $scope.getTotal = function () {
        return $scope.notaVenta.length > 0 ?
            $scope.notaVenta.reduce((acc, curr) => acc + curr.precioVenta * curr.cantidad, 0) : 0;
    }

    $scope.setCantidadMaxima = function () {
        let prod = $scope.productos.find(element => element.idPRODUCTOS == $scope.producto.id);
        $scope.cantidadMaxima = prod.cantidad;
    }

    $scope.eliminar = function (obj) {
        $scope.notaVenta = $scope.notaVenta.filter(element => element !== obj);
    }

    $scope.mostrarDatosModal = function () {
        $('#agregarProdctoVenta').modal('show');
    };

    $scope.completarCompra = function () {
        if ($scope.notaVenta.length === 0) {
            alert('Agrega productos a la nota de venta');
        } else {
            apiVentas.postVenta($scope.notaVenta, $scope.idCliente, $scope.idEmpleado).then(() => {
                apiProductos.getProductos().then(function (response) {
                    $scope.productos = response[0];
                    $scope.$apply();
                });
            });
        }
    };

    apiProductos.getProductos().then(function (response) {
        $scope.productos = response[0];
        $scope.$apply();
    });

    apiCliente.getAllClientes().then(function (response) {
        $scope.clientes = response[0];
        $scope.$apply();
    });

    apiEmpleados.getAllEmpleados().then(function (response) {
        $scope.empleados = response;
        $scope.$apply();
    });
});


