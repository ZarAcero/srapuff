import { app } from '../moduleAngular.js';

app.controller('clientesController', function ($scope, apiCliente, apiEdo, apiCodPostal, apiPaqueteria) {

    $scope.clienteModal = {
        idClientes: '', Nombre: '', Ape_Pa: '', Ape_Ma: '', RFC: '', Telefono: '', idCodPostal: '', idEdo: '', Celular: '', Observaciones: '', Dir: '', Activo: ''
    }
    $scope.cliente = {
        idClientes: '', Nombre: '', Ape_Pa: '', Ape_Ma: '', RFC: '', Telefono: '', idCodPostal: '', idEdo: '', Celular: '', Observaciones: '', Dir: '', Activo: ''
    }
    $scope.paqueteriaSelected;

    $scope.estados;
    $scope.cp;
    $scope.paqueterias;

    $scope.mostrarAlerta = false

    function validate(cliente) {
        return (
            cliente.Nombre === '' ||
            cliente.RFC === '' ||
            cliente.idCodPostal === '' ||
            cliente.idEdo === ''
        ) ? false : true;
    }

    $scope.agregarCliente = function () {
        let formAgregar = document.getElementById('collapseAddCliente')
        formAgregar.classList.remove('show');
        var textLoad = document.getElementById('cargando');
        if (textLoad.classList.contains("d-none")) textLoad.classList.remove("d-none");
        if (validate($scope.cliente)) {
            apiCliente.insertCliente($scope.cliente, $scope.paqueteriaSelected).then(function (response) {
                console.log($scope.cliente);
                $scope.cliente = {
                    idClientes: '', Nombre: '', Ape_Pa: '', Ape_Ma: '', RFC: '', Telefono: '', idCodPostal: '', idEdo: '', Celular: '', Observaciones: '', Dir: '',
                }
                apiCliente.getAllClientes().then(function (response) {
                    $scope.clientes = response[0]
                    textLoad.classList.add("d-none");

                    $scope.$apply()
                });
            }, function (err) {
                console.log(err)
            })

        } else {
            alert('Llena los campos!')
        }
    }

    $scope.eliminarCliente = function (id) {
        var textLoad = document.getElementById('cargando');
        if (textLoad.classList.contains("d-none")) textLoad.classList.remove("d-none");
        apiCliente.deleteCliente(id).then(function (response) {
            apiCliente.getAllClientes().then(function (response) {
                textLoad.classList.add("d-none");
                $scope.clientes = response[0]
                $scope.$apply();
            });
        })
    }

    $scope.mostrarDatosModal = function (id) {
        apiCliente.getCliente(id).then(function (response) {
            $scope.clienteModal = response[0]
            $scope.$apply(function () {
                $('#editarClienteModal').modal('show');
            });
        })
    }

    $scope.updateCliente = function () {
        var textLoad = document.getElementById('cargando');
        if (textLoad.classList.contains("d-none")) textLoad.classList.remove(".d-none");
        if (!validate($scope.clienteModal)) {
            $scope.mostrarAlerta = true
        } else {
            apiCliente.updateCliente($scope.clienteModal ,$scope.paqueteriaSelected).then(function (response) {
                apiCliente.getAllClientes().then(function (response) {
                    $('#editarClienteModal').modal('hide');
                    textLoad.classList.add("d-none");
                    $scope.clientes = response[0]
                    $scope.$apply();
                });
            })
        }
    }

    //Inicializacion del controlador
    apiEdo.getAllEdos().then(function (response) {
        $scope.estados = response;
        apiCodPostal.getAllCodPostal().then(function (response) {
            $scope.cp = response;
            apiCliente.getAllClientes().then(function (response) {
                $scope.clientes = response[0];
                console.log($scope.clientes);
                apiPaqueteria.getPaqueterias().then((r) => {
                    $scope.paqueterias = r;
                    let textLoad = document.getElementById('cargando');
                    textLoad.classList.add("d-none");
                    $scope.$apply()
                });
            });
        });
    });
});

