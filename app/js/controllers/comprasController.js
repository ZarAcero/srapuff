import { app } from '../moduleAngular.js';

app.controller('comprasController', function ($scope, apiProductos, apiProveedor, apiCompras) {

    $scope.notaVenta = [];
    $scope.producto;
    $scope.productos;
    $scope.proveedores;
    $scope.idProveedor;

    $scope.agregarProducto = function () {
        apiProductos.getProducto($scope.producto.id).then((response) => {
            $scope.notaVenta.push({
                id: $scope.producto.id,
                nombre: response[0].nombre,
                descripcion: response[0].descripcion,
                precioCompra: response[0].precioCompra,
                cantidad: $scope.producto.cantidad
            });
            $scope.producto = {}
            $scope.comprasModalForm.$setUntouched()
            $scope.$apply();
        });
    }

    $scope.registrarCompra = function () {
        if ($scope.notaVenta.length === 0) {
            alert('Agrega productos a la nota de compra');
        } else {
            apiCompras.postCompra($scope.notaVenta, $scope.idProveedor);
        }
    }

    $scope.getTotal = function () {
        return $scope.notaVenta.length > 0 ?
            $scope.notaVenta.reduce((acc, curr) => acc + curr.precioCompra * curr.cantidad, 0) : 0;
    }

    $scope.mostrarDatosModal = function () {
        $('#agregarProdctoCompra').modal('show');
    }

    $scope.eliminar = function (obj) {
        $scope.notaVenta = $scope.notaVenta.filter((element) => {
            return element !== obj;
        });
    }

    apiProveedor.getAllProveedores().then(function (response) {
        $scope.proveedores = response[0];
        $scope.$apply();
    });

    apiProductos.getProductos().then(function (response) {
        $scope.productos = response[0];
    })

});