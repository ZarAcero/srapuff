import { app } from '../moduleAngular.js';

app.controller('paqueteriaController', function ($scope, apiPaqueteria) {

    $scope.listaPaqueterias = [];
    $scope.paqueteria;
    $scope.paqueteriaAEditar;


    $scope.agregarPaqueteria = function () {
        apiPaqueteria.postPaqueteria([$scope.paqueteria.nombre, $scope.paqueteria.precio, $scope.paqueteria.observaciones]);
        $scope.paqueteria = {};
        $scope.agregarPaqueteriaForm.$setUntouched()
        apiPaqueteria.getPaqueterias().then((response) => {
            $scope.listaPaqueterias = response;
            $scope.$apply();
        });
    }

    $scope.agregarPaqueteriaModal = function () {
        $('#agregarPaqueteriaModal').modal('show')
    }

    $scope.eliminar = function (id) {
        apiPaqueteria.deletePaqueteria(id).then(() => {
            apiPaqueteria.getPaqueterias().then((response) => {
                $scope.listaPaqueterias = response;
                $scope.$apply();
            });
        });
    }

    $scope.mostrarModalEditar = function (id) {
        apiPaqueteria.getPaqueteria(id).then((response) => {
            $scope.paqueteriaAEditar = response[0];
            $scope.$apply(function () {
                $('#editarPaqueteriaModal').modal('show');
            });
        });
    }

    $scope.editarPaqueteria = function (id) {
        apiPaqueteria.putPaqueteria([$scope.paqueteriaAEditar.Nombre, $scope.paqueteriaAEditar.precio_kilo, $scope.paqueteriaAEditar.observaciones, $scope.paqueteriaAEditar.idPaqueteria]).then((response) => {
            apiPaqueteria.getPaqueterias().then((response) => {
                $scope.listaPaqueterias = response;
                $scope.$apply();
                $('#editarPaqueteriaModal').modal('hide');
            });
        });
    }

    apiPaqueteria.getPaqueterias().then((response) => {
        $scope.listaPaqueterias = response;
        $scope.$apply();
    });
});