import { app } from './moduleAngular.js'
import { pool } from '../database/connection.js'

app.factory('apiCliente', function () {
    return {
        getCliente: function (id) {
            return pool.query('SELECT idClientes, Nombre, Ape_Pa, Ape_Ma, RFC, Telefono, idCodPostal, idEdo, Celular, Observaciones, Dir FROM clientes WHERE idClientes = ?', id)
        },
        updateCliente: function (cliente, idPaqueteria) {
            var params = [cliente.Nombre, cliente.Ape_Pa, cliente.Ape_Ma, cliente.RFC, cliente.Telefono, Number(cliente.idCodPostal), Number(cliente.idEdo), cliente.Celular, cliente.Observaciones, cliente.Dir, cliente.idClientes]
            return new Promise((resolve,reject)=>{
                pool.getConnection((err,connection)=>{
                    connection.beginTransaction((err)=>{
                        if(err){
                            alert(err);
                            reject(0);
                        } else {
                            connection.query('UPDATE clientes SET Nombre=?, Ape_Pa=?, Ape_Ma=?, RFC=?, Telefono=?, idCodPostal=?, idEdo=?, Celular=?, Observaciones=?, Dir=? WHERE idClientes=?', params,(err,results)=>{
                                if(err){
                                    alert(err);
                                    connection.rollback();
                                    reject(0);
                                } else {
                                    connection.query('UPDATE paq_clientes SET idPaqueteria = ? WHERE idClientes = ?;', [idPaqueteria, cliente.idClientes],(err,results)=>{
                                        if(err){
                                            alert(err);
                                            connection.rollback();
                                            reject(0);
                                        } else {
                                            connection.commit();
                                            alert('Usuario actualizado');
                                            resolve(1);
                                        }
                                    })
                                }
                            });
                        }
                    });
                });
            });
            
            //return pool.query('UPDATE clientes SET Nombre=?, Ape_Pa=?, Ape_Ma=?, RFC=?, Telefono=?, idCodPostal=?, idEdo=?, Celular=?, Observaciones=?, Dir=? WHERE idClientes=?', params)
        },
        insertCliente: function (cliente, idPaqueteria) {
            var params = [cliente.Nombre, cliente.Ape_Pa, cliente.Ape_Ma, cliente.RFC, cliente.Telefono, Number(cliente.idCodPostal), Number(cliente.idEdo), cliente.Celular, cliente.Observaciones, cliente.Dir, Number(cliente.idCodPostal), Number(cliente.idEdo), 1]
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    connection.beginTransaction((err) => {
                        if (err) {
                            alert(err);
                            reject(0)
                        } else {
                            connection.query('INSERT INTO clientes(Nombre, Ape_Pa, Ape_Ma, RFC, Telefono, idCodPostal, idEdo, Celular, Observaciones, Dir, ACTIVO) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)', params, (err, results) => {
                                if (err) {
                                    alert(err.message);
                                    connection.rollback();
                                    reject(0)
                                } else {
                                    connection.query('INSERT INTO paq_clientes(idPaqueteria,idClientes) VALUES(?,?)', [idPaqueteria, results.insertId], (err) => {
                                        if (err) {
                                            alert(err);
                                        } else {
                                            connection.commit();
                                            alert('Usuario agregado');
                                            resolve(1);
                                        }
                                    })
                                }
                            });
                        }
                    })
                    connection.release();
                });
            });
        },
        getAllClientes: function () {
            return pool.query('CALL getAllUser()');
        },
        deleteCliente: function (id) {
            return pool.query('UPDATE clientes SET ACTIVO=0 WHERE idClientes=?', id)
        }
    }
})

app.factory('apiEdo', function () {
    return {
        getAllEdos: function () {
            return pool.query('SELECT * FROM edo;')
        }
    }
})


app.factory('apiCodPostal', function () {
    return {
        getAllCodPostal: function () {
            return pool.query('SELECT * FROM codpostal;')
        }
    }
})

app.factory('apiProveedor', function () {
    return {
        getProveedor: function (id) {
            return pool.query('SELECT idProveedor, nombre, telefono, celular, RFC, Correo, observaciones from proveedores WHERE idProveedor = ?', id)
        },
        updateProveedor: function (proveedor) {
            var params = [proveedor.nombre, proveedor.Telefono, proveedor.Celular, proveedor.RFC, proveedor.Correo, proveedor.Observaciones, proveedor.idProveedor]
            return pool.query('UPDATE proveedores SET nombre=?, telefono=?, celular=?, RFC=?, correo=?, Observaciones=? WHERE idProveedor=?', params)
        },
        insertProveedor: function (proveedor) {
            console.log('ANtes de insertar',proveedor);
            var params = [proveedor.nombre, proveedor.Telefono, proveedor.Celular, proveedor.RFC, proveedor.Correo, proveedor.Observaciones, 1]
            return pool.query('INSERT INTO proveedores(nombre,telefono,celular,RFC,correo,observaciones,activo) VALUES(?,?,?,?,?,?,?)', params)
        },
        getAllProveedores: function () {
            return pool.query('CALL getAllProveedores();')
        },
        deleteProveedor: function (id) {
            return pool.query('UPDATE proveedores SET ACTIVO=0 WHERE idProveedor=?', id)
        }
    }

})

app.factory('apiProductos', function () {
    return {
        getProductos: function () {
            return pool.query('CALL getAllProductos();');
        },
        getProducto: function (id) {
            return pool.query('SELECT nombre, descripcion, precioVenta, precioCompra, activo from productos WHERE idPRODUCTOS = ?', id);
        },
        insertProducto: function (producto) {
            var params = [producto.nombre, producto.descripcion, producto.idTalla, producto.precioVenta, producto.precioCompra, 1];
            return new Promise((resolve, reject) => {
                pool.getConnection((err, concection) => {
                    concection.beginTransaction((err) => {
                        if (err) {
                            alert(err.message);
                            reject(0);
                        } else {
                            concection.query('INSERT INTO productos(nombre, descripcion, idTalla, precioVenta, precioCompra, activo) VALUES (?,?,?,?,?,?)', params, (err, results) => {
                                if (err) {
                                    alert(err.message)
                                    concection.rollback();
                                    reject(0);
                                } else {
                                    concection.query('INSERT INTO INVENTARIO(idProductos,cantidad) VALUES(?,?)', [results.insertId, producto.cantidad], (err, results) => {
                                        if (err) {
                                            alert(err.message)
                                            concection.rollback();
                                            reject(0);
                                        } else {
                                            alert('Producto agregado');
                                            concection.commit();
                                            resolve(1);
                                        }
                                    });
                                }
                            });
                        }
                    })
                    concection.release();
                });
            });
        },
        updateProducto: function (producto) {
            var params = [producto.nombre, producto.descripcion, producto.idTalla, producto.precioVenta, producto.precioCompra, producto.idPRODUCTOS];
            return new Promise((resolve, reject) => {
                pool.getConnection((err, concection) => {
                    concection.beginTransaction((err) => {
                        if (err) {
                            alert(err.message);
                            concection.rollback();
                            reject(0);
                        } else {
                            concection.query('UPDATE productos SET nombre=?, descripcion=?, idTalla=?, precioVenta=?, precioCompra=? WHERE idPRODUCTOS=?', params, (err, results) => {
                                if (err) {
                                    alert(err.message);
                                    concection.rollback();
                                    reject(0);
                                } else {
                                    concection.query('UPDATE INVENTARIO SET cantidad = ? WHERE idProductos = ?;', [producto.cantidad, producto.idPRODUCTOS], (err, results) => {
                                        if (err) {
                                            alert(err.message);
                                            concection.rollback();
                                            reject(0);
                                        } else {
                                            alert('Producto actualizado');
                                            concection.commit();
                                            resolve(1);
                                        }
                                    });
                                }
                            });
                        }
                    });
                    concection.release();
                });
            })
        },
        deleteProducto: function (id) {
            return pool.query('UPDATE productos SET activo=0 WHERE idPRODUCTOS=?', id);
        }
    }
})

app.factory('apiTallas', function () {
    return {
        getAllTallas: function () {
            return pool.query('SELECT * FROM Tallas');
        }
    }
})

app.factory('apiCompras', function () {
    return {
        postCompra: function (notaVenta, idProveedor) {
            pool.getConnection((err, concection) => {
                concection.beginTransaction((err) => {
                    if (err) {
                        alert(err.message);
                    } else {
                        let d = new Date()
                        let fecha = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`
                        concection.query(`INSERT INTO compras(idProveedor,fecha) VALUES(?,?);`, [idProveedor, fecha], (err, results) => {
                            if (err) {
                                alert(err.message);
                                concection.rollback();
                            } else {
                                let allOk = true;
                                let promesas = []
                                notaVenta.forEach((element) => {
                                    promesas.push(concection.query('INSERT INTO compra_prod(idCompra,idProductos,cantidad) VALUES(?,?,?);', [results.insertId, element.id, element.cantidad], (err) => {
                                        if (err) {
                                            alert(err.message);
                                            allOk = false;
                                            concection.rollback();
                                        }
                                    }))
                                });
                                Promise.all(promesas).then(() => {
                                    if (allOk) {
                                        concection.commit((err) => {
                                            if (err) {
                                                alert(err.message)
                                                concection.rollback();
                                            } else {
                                                alert('Compra registrada');
                                            }
                                        });
                                    }
                                });
                            }
                        })
                    }
                })
                concection.release();
            })
        }
    }
});

app.factory('apiVentas', function () {
    return {
        postVenta: function (notaVenta, idCliente, idEmpleado) {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    connection.beginTransaction((err) => {
                        if (err) {
                            alert(err.message);
                        } else {
                            let d = new Date()
                            let fecha = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`
                            connection.query('INSERT INTO ventas(idCliente,fecha,idEmpleado) VALUES(?,?,?);', [idCliente, fecha, idEmpleado], (err, results) => {
                                if (err) {
                                    alert(err.message);
                                    connection.rollback();
                                    reject(0);
                                } else {
                                    let allOk = true;
                                    let promesas = [];
                                    notaVenta.forEach((element) => {
                                        promesas.push(connection.query('INSERT INTO venta_prod(idVenta,idProductos,cantidad) VALUES(?,?,?);', [results.insertId, element.id, element.cantidad], (err) => {
                                            if (err) {
                                                allOk = false;
                                                alert(err.message);
                                                connection.rollback();
                                                reject(0);
                                            }
                                        }))
                                        Promise.all(promesas).then(() => {
                                            if (allOk) {
                                                connection.commit();
                                                resolve(1);
                                                alert('Venta realizada');
                                            }
                                        })
                                    });
                                }
                            });
                        }
                        connection.release();
                    });
                });
            });
        }
    }
});

app.factory('apiEmpleados', function () {
    return {
        getAllEmpleados: function () {
            return pool.query('SELECT * FROM empleados WHERE activo = 1;');
        },
        getEmpleado: function (id) {
            return pool.query('SELECT * FROM empleados WHERE idEmpleado = ?', id);
        },
        postEmpleado: function (params) {
            return pool.query('INSERT INTO empleados(nombre,Ape_Pa,Ape_Ma,activo) VALUES(?,?,?,1);', params);
        },
        putEmpleado: function (params) {
            return pool.query('UPDATE empleados SET nombre = ?, Ape_Pa = ?, Ape_Ma = ? WHERE idEmpleado = ?;', params);
        },
        deleteEmpleado: function (id) {
            return pool.query('UPDATE empleados SET activo = 0 WHERE idEmpleado = ?; ', id);
        }
    }
});

app.factory('apiPaqueteria', function () {
    return {
        postPaqueteria: function (params) {
            return pool.query('INSERT INTO paqueterias(Nombre,precio_kilo,observaciones,activo) VALUES(?,?,?,1);', params);
        },
        getPaqueterias: function () {
            return pool.query('SELECT * FROM paqueterias WHERE activo = 1;');
        },
        getPaqueteria: function (id) {
            return pool.query('SELECT * FROM paqueterias WHERE idPaqueteria = ?', id);
        },
        putPaqueteria: function (params) {
            return pool.query('UPDATE paqueterias SET Nombre = ?, precio_kilo = ?,observaciones = ? WHERE idPaqueteria = ?;', params);
        },
        deletePaqueteria: function (id) {
            return pool.query('UPDATE paqueterias SET activo = 0 WHERE idPaqueteria = ?', id);
        },
        insertPaqueteriaAUsuario: function (idCliente, idPaqueteria) {
            return pool.query('INSERT INTO paq_clientes(idPaqueteria,idClientes) VALUES(?,?)', [idPaqueteria, idCliente]);
        },
        updatePaqueteriaAUsuario: function (idCliente, idPaqueteria) {
            return pool.query('UPDATE paq_clientes SET idPaqueteria = ? WHERE idClientes = ?;', [idPaqueteria, idCliente]);
        }
    }
});

app.factory('apiConsultas', function (){
    return{
        masVentasCliente: function(){
            return pool.query('select * from masVentasCliente;');
        },
        todosLosProductos: function(){
            return pool.query('select * from todosLosProductos;');
        },
        getVentas: function(){
            let f = new Date()
            let fecha = `${f.getFullYear()}-${f.getMonth()+1}-${f.getDate()}`;
            console.log(fecha);
            return pool.query(`SELECT productos.nombre, sum(venta_prod.cantidad) AS total,(sum(venta_prod.cantidad)*productos.precioVenta) AS ingreso
            FROM productos, venta_prod, ventas
            WHERE productos.idPRODUCTOS = venta_prod.idProductos 
                AND ventas.idVenta = venta_prod.idVenta
                AND ventas.fecha 
                    BETWEEN CAST('${fecha} 00:00:00' AS DATETIME) 
                        AND CAST('${fecha} 23:59:59' AS DATETIME)
            GROUP BY productos.idPRODUCTOS, productos.nombre;`);
        },
        getVentasGlobales: function(){
            return pool.query(`SELECT productos.nombre, sum(venta_prod.cantidad) AS total
            FROM productos, venta_prod, ventas
            WHERE productos.idPRODUCTOS = venta_prod.idProductos 
                AND ventas.idVenta = venta_prod.idVenta
            GROUP BY productos.idPRODUCTOS, productos.nombre with rollup;`);
        }
    }
});