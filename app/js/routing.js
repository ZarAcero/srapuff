import { app } from './moduleAngular.js';

app.config(function ($routeProvider) {
    var basePath = '../app/views/'

    $routeProvider
        .when("/", {
            templateUrl: basePath + 'main.html',
            controller: "homeController"
        })
        .when('/clientes', {
            templateUrl: basePath + 'clientes.html',
            controller: 'clientesController'
        })
        .when('/proveedores', {
            templateUrl: basePath + 'proveedores.html',
            controller: 'proveedoresController'
        })
        .when('/compras', {
            templateUrl: basePath + 'compras.html',
            controller: 'comprasController'
        })
        .when('/ventas', {
            templateUrl: basePath + 'ventas.html',
            controller: 'ventasController'
        })
        .when('/productos', {
            templateUrl: basePath + 'productos.html',
            controller: 'productosController'
        })
        .otherwise({
            templateUrl: basePath + 'error.html'
        });
});